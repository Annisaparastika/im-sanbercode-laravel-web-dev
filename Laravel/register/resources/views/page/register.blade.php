@extends('layouts.master')

@section('title')
Halaman Utama
@endsection

@section('subtitle')
ini subtile halaman utama
@endsection

@section('content')


    <h1>Sign Up Form</h1>

    <form action="/kirim" method="post">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last Name</label><br><br>
        <input type="text"name="lname"> <br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="male">Male<br>
        <input type="radio"name="female">Female<br>
        <input type="radio"name="other">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality" id="">
            <option value=""name="indo">Indonesian</option>
            <option value=""name="malay">Malaysian</option>
            <option value=""name="singapore">Singaporean</option>
            <option value=""name="aus">Australian</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox"name="eng">English<br>
        <input type="checkbox" name="Lother">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="textarea" rows="10"></textarea>
        <br><br>

        <input type="submit"value="kirim">
    </form>
@endsection
