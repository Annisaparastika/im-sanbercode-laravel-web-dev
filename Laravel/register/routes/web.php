<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController:: class, 'Home']);
Route::get('/register', [AuthController:: class, 'Register']);
Route::post('/kirim', [AuthController:: class, 'Kirim']);



Route::get('/table', function(){
    return view ('page.table');
});

Route::get('/dataTable', function(){
    return view ('page.dataTable');
});

