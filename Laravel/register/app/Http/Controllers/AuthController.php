<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register()
    {
        return view('page.register');
    }
    public function Kirim(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('page.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
